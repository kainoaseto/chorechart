<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-secale=1">
    <meta name="description" content="Chore Chart to manage The Incubator">
    <meta name="author" content="Kainoa Seto">

    <title>Chore Chart</title>

    <link rel="stylesheet" type="text/css" href="node_modules/morris.js/morris.css"></script>

    <!-- Start page specific stylesheets -->
    <link rel="stylesheet" type="text/css" href="stylesheets/css/main.css">
    <!-- End page specific stylesheets -->
</head>
