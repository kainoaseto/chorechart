chores = ["Clean Surfaces", "Sweep/Clean Floors", "Trash/Recycle Takeout", "Clean Bathroom"]
groups = ["Tyler & Sam","Jacob & Evan","Brennon & Will","Kainoa & Maurice"]
daily_chores = ["Clean Dishes"]
daily_groups = ["Tyler & Sam","Jacob & Evan","Brennon & Will","Kainoa & Maurice"]
/*CreateDonut = function() {
    Morris.Donut({
      element: 'chore-donut',
      data: [
        {label: chores[2], value: 25},
        {label: chores[1], value: 25},
        {label: chores[0], value: 25},
        {label: chores[3], value: 25}
      ],
      formatter: function(y, data) {
          chore = data['label'];
          for(var i = 0; i < 4; i++)
          {
              if(chore == chores[i]) {
                  return groups[i];
              }
          }
      },
      colors: ['#008744', '#0057e7', '#d62d20', '#ffa700']
    });
}*/



$(document).ready(function() {
    $.ajax({
            type: "JSON",
            url: "php/get_groups.php",
            success: function(response) {
                result = response.split("|");
                groups = JSON.parse(result[0]);
                reset_time = parseInt(result[1]);
                date_reset_time = moment(reset_time*1000).format("MM-DD-YYYY HH:mm:ss");
                $('#chore1').text(chores[0]);
                $('#chore2').text(chores[1]);
                $('#chore3').text(chores[2]);
                $('#chore4').text(chores[3]);


                $('#room1').text(groups[0]);
                $('#room2').text(groups[1]);
                $('#room3').text(groups[2]);
                $('#room4').text(groups[3]);
                $('#current').html("Current Date: " + moment().format("MM-DD-YYYY HH:mm:ss"));
                $('#cycle').html("Cycles on: " + date_reset_time);
            }
        });

    $.ajax({
            type: "JSON",
            url: "php/get_daily_groups.php",
            success: function(response) {
                result = response.split("|");
                daily_groups = JSON.parse(result[0]);
                reset_time = parseInt(result[1]);
                date_reset_time = moment(reset_time*1000).format("MM-DD-YYYY HH:mm:ss");
                $('#daily_chore1').text(daily_chores[0]);
                $('#daily_room1').text(daily_groups[0]);
                $('#current').html("Current Date: " + moment().format("MM-DD-YYYY HH:mm:ss"));
                $('#cycle').html("Cycles on: " + date_reset_time);
            }
        });


    console.log("Javascript loaded")
});
