<html xmlns="http://www.w3.org/1999/html">

<!-- Start <head> -->
<?php include("partials/header.php"); ?>
<!-- End <head> -->

<body>
    <!--<div id="chore-donut"></div>-->

    <div id="chore-grid">
        <div id="row">
            <div class="col-sm-6">
                <div class="row">
                    <h2 id="chore1" class="centered">Chore 1</h2>
                </div>
                <div class="row">
                    <h3 id="room1" class="centered">Person 1 & person 2</h3>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <h2 id="chore2" class="centered">Chore 2</h2>
                </div>
                <div class="row">
                    <h3 id="room2" class="centered">Person 3 & person 4</h3>
                </div>
            </div>
        </div>
        <div id="row">
            <div class="col-sm-6">
                <div class="row">
                    <h2 id="chore3" class="centered">Chore 3</h2>
                </div>
                <div class="row">
                    <h3 id="room3" class="centered">Person 5 & person 6</h3>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <h2 id="chore4" class="centered">Chore 4</h2>
                </div>
                <div class="row">
                    <h3 id="room4" class="centered">Person 7 & person 8</h3>
                </div>
            </div>
        </div>
        <div id="row">
            <div class="col-sm-12">
                <div class="row">
                    <h2 id="daily_chore1" class="centered">Daily Chore 1</h2>
                </div>
                <div class="row">
                    <h3 id="daily_room1" class="centered">Person 7 & person 8</h3>
                </div>
            </div>
        </div>
    </div>

    <div id="info">
        <h2 id="current" class="centered">Current:</h2>
        <h2 id="cycle" class="centered">Cycle on:</h2>
    </div>

    <!--Start Footer-->
    <?php include("partials/footer.php"); ?>
    <!--End Footer-->

</body>
</html>
