# README #

This is the Incubator chore chart that rotates on a weekly basis to keep track of who has what chore

Check it out at http://chorechart.xyz

### How do I get set up? ###

* Clone into a folder and run npm install to setup the project's dependencies
* You will need to use Gulp or some other Sass compiler to compile the sass into css that should be located in /stylesheets/css