// Include gulp
var gulp = require('gulp');

// Include Our plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var open = require('gulp-open');

// Lint Task
gulp.task('lint', function() {
	return gulp.src('js/*.js')
			.pipe(jshint())
			.pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
	return gulp.src('stylesheets/sass/*.scss')
			.pipe(sass())
			.pipe(gulp.dest('stylesheets/css'));
});

// Cocatenate & Minify JS
gulp.task('scripts', function() {
	return gulp.src('js/*.js')
			.pipe(concat('scripts.js'))
			.pipe(gulp.dest('dist'))
			.pipe(rename('scripts.min.js'))
			.pipe(uglify())
			.pipe(gulp.dest('dist'));
});

// Watch Files for changes
gulp.task('watch', function() {
	gulp.watch('js/*.js', ['lint', 'scripts']);
	gulp.watch('stylesheets/sass/*.scss', ['sass']);
});

gulp.task('open', function() {
	var options = {
		uri: 'http://localhost/chorechart/',
		app: 'firefox'
	};
	gulp.src(__filename)
	.pipe(open(options));
})

// Default (Build) Task
gulp.task('default', ['lint', 'sass', 'scripts', 'watch']);

// Test (Run) Task
gulp.task('run', ['open']);

// npm install gulp gulp-concat gulp-jshint gulp-open gulp-rename gulp-sass gulp-uglify
