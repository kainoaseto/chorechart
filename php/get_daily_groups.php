<?php

    $daily_chore_groups = fopen("../daily_groups.json", "r") or die("Unable to open file!");
    $daily_groups = json_decode(fgets($daily_chore_groups));
    $daily_reset_date = fgets($daily_chore_groups);

    if(time() - $daily_reset_date >= 0)
    {
        fclose($daily_chore_groups);
        $daily_reset_date += 86400;
        $last = $daily_groups[count($daily_groups) - 1];
        for($i = count($daily_groups) - 1; $i > 0; $i--) {
            $daily_groups[$i] = $daily_groups[$i - 1];
        }
        $daily_groups[0] = $last;
        unlink("../daily_groups.json");
        $daily_chore_groups = fopen("../daily_groups.json", "w") or die("Unable to create file!");
        fwrite($daily_chore_groups, json_encode($daily_groups) . "\n");
        fwrite($daily_chore_groups, $daily_reset_date);
    }

    echo json_encode($daily_groups) . "|" . $daily_reset_date;
    fclose($daily_chore_groups);
?>
