<?php

    $chore_groups = fopen("../groups.json", "r") or die("Unable to open file!");
    $groups = json_decode(fgets($chore_groups));
    $reset_date = fgets($chore_groups);


    if(time() - $reset_date >= 0)
    {
        fclose($chore_groups);
        $reset_date += 604800;
        $last = $groups[count($groups) - 1];
        for($i = count($groups) - 1; $i > 0; $i--) {
            $groups[$i] = $groups[$i - 1];
        }
        $groups[0] = $last;
        unlink("../groups.json");
        $chore_groups = fopen("../groups.json", "w") or die("Unable to create file!");
        fwrite($chore_groups, json_encode($groups) . "\n");
        fwrite($chore_groups, $reset_date);
    }

    echo json_encode($groups) . "|" . $reset_date;
    fclose($chore_groups);
?>
